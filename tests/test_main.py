from io import StringIO
import unittest
from unittest import TestCase
from unittest.mock import patch, mock_open, Mock, call

from main import Bathroom, Main


class TestBathroom(TestCase):
    @patch('builtins.open', mock_open(read_data="Welcome in bath"))  # Patching reading files
    def test_creating_bathroom(self):
        with patch('builtins.print', new_callable=Mock()) as m:  # Patching output
            Bathroom()  # Run code to test it
        self.assertEqual(m.call_count, 1, "There was no single print!")
        self.assertEqual(m.call_args, call("Welcome in bath"), "Incorrect greetings in bath!")


class TestMain(TestCase):
    @patch('builtins.input', side_effect=['', 'wyjdz'])  # Mock input from user with unknown command
    def test_main_loop_check_file_exists(self, input_mock):
        main = Main()
        main.loop()  # If code is cool it should not raise any exception
        self.assertEqual(input_mock.call_count, 2)  # Check there was input called on start, and after wrong command
        # Check last run input command
        self.assertEqual(input_mock.call_args, call("Nie znam takiej komendy podaj poprawna:\n"))


if "__main__" == __name__:
    unittest.main()