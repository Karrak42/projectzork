Podstawowe komendy:

zycie - pokazuje aktualny stan zdrowia podstawowo 100hp
kierunek - pokazuje w jakim kierunku jesteś skierowany
ekwipunek - pokazuje ekwipunek 
sprawdz + rzecz - sprawdza obiekt
otworz + rzecz - otwiera dana rzecz
pomoc - pokazuje menu pomocy z komendami
dziennik - pokazuje aktualne zadania. Podstawowo:

przedrostek idz +:
lewo - idz w lewo
prawo - idz w prawo
przod - idz w przod
tyl - idz w tyl / wyjdz z pomieszczenia

kierunki powyzej stosowane sa w pomieszczeniach zamknietych
na zewnatrz nalezy stosowac kierunki swiata, czyli:

polnoc - idz na polnoc
poludnie - idz na poludnie
zachod - idz na zachod
wschod - idz na wschod

pisz bez polskich znakow!