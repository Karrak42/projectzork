# !/usr/bin/env python3

__author__ = "Maciej Palubicki"
__copyright__ = "Copyright (c) Maciej Palubicki"
__email__ = "palubickimaciej5@gmail.com"
__version__ = "1.0"

from random import randrange


class Player:

    def __init__(self):
        self.hp: int = 100
        self.side: str = "Polnoc"
        self.key_status: bool = False
        self.crowbar_status: bool = False
        self.evidence: int = 0
        self.motive: int = 0
        self.is_in_room: bool = False
        self.eq = []
        self.door_status: bool = False


class Final:
    def __init__(self):
        self.name: str = "Final"
        with open("src/final.txt") as self.description:
            print(self.description.read())


class North_home:
    def __init__(self):
        self.name: str = "Polnoc"
        with open("src/north_home.txt") as self.description:
            print(self.description.read())


class East_home:
    def __init__(self):
        self.name: str = "Zachod domu"

        with open("src/west_home.txt", 'r', 'utf-8') as self.description:
            print(self.description.read())


class West_home:
    def __init__(self):
        self.name: str = "Wschod domu"
        with open("src/west_home.txt") as self.description:
            print(self.description.read())


class South_home:
    def __init__(self):
        self.name: str = "Poludnie domu"
        with open("src/south_home.txt") as self.description:
            print(self.description.read())


class House_corridor_upper:
    def __init__(self):
        self.name: str = "korytarz gora"
        with open("src/corridor_up.txt") as self.description:
            print(self.description.read())


class Kitchen:
    def __init__(self):
        self.name: str = "kuchnia"
        with open("src/kitchen.txt") as self.description:
            print(self.description.read())


class Bedroom:
    def __init__(self):
        self.name: str = "sypialnia"
        with open("src/bedroom.txt") as self.description:
            print(self.description.read())


class Bathroom:
    def __init__(self):
        self.name: str = "lazienka"
        with open("src/bathroom.txt") as self.description:
            print(self.description.read())


class Main:
    start_input = input(
        "Witaj w grze Dom Corbitta!\nAby zacząć grę napisz start. Jeśli nie znasz komend napisz pomoc. Jeśli chcesz "
        "wyjść z gry napisz wyjdz:\n").lower()

    while start_input != "wyjdz":
        if start_input == "start":
            with open("src/start_game.txt") as start:
                print(start.read())
            player = Player()

            while start_input != "wyjdz":

                """The beginning of the game in front of the house NORTH"""
                while player.side == "Polnoc":
                    if not player.is_in_room:
                        North_home()
                        player.is_in_room = True
                    start_input = input().lower()
                    if player.evidence >= 3 and player.motive == 1:
                        player.side = "Final"
                    elif start_input == "idz zachod":
                        print("Idziesz na zachód domu:\n")
                        player.side = "Zachod"
                        player.is_in_room = False
                    elif start_input == "idz wschod":
                        print("Idziesz na wschód domu:\n")
                        player.side = "Wschod"
                        player.is_in_room = False
                    elif start_input == "idz poludnie":
                        print("na południu nic nie ma:\n")
                    elif start_input == "idz polnoc":
                        print("stoisz juz w kierunku północnym:\n")
                    elif start_input == "sprawdz wycieraczka":
                        player.key_status = True
                        player.eq.append("Klucz do drzwi")
                        print("Pod wycieraczką znajdujesz klucz prawdopodobnie do drzwi. Bierzesz go ze sobą:\n")
                    elif start_input == "sprawdz doniczka":
                        print("Sprawdzasz doniczkę i okazuje się że tam nic nie ma:\n")
                    elif start_input == "otworz drzwi":
                        if player.key_status:
                            print("Otwierasz drzwi znalezionym kluczem i wchodzisz do domu:\n")
                            player.side = "Dom_korytarz_gora"
                            player.is_in_room = False
                        else:
                            print("Nie możesz otworzyć drzwi bez klucza:\n")
                    elif start_input == "sprawdz drzwi":
                        print("Widzisz masyne drzwi można je otworzyć jedynie kluczem:\n")
                    elif start_input == "pomoc":
                        with open("src/help.txt") as f:
                            print(f.read() + "\n")
                    elif start_input == "wyjdz":
                        exit()
                    elif start_input == "dziennik":
                        print(player.journal)
                    elif start_input == "zycie":
                        print("Życie wynosi: {}".format(player.hp))
                    elif start_input == "ekwipunek":
                        print("Ekwipunek: {}".format(player.eq))
                    else:
                        print("nie znam takiej komendy podaj poprawna:\n")

                """West side of the home"""

                while player.side == "Zachod":
                    if not player.is_in_room:
                        West_home()
                        player.is_in_room = True
                    start_input = input().lower()
                    if player.evidence >= 3 and player.motive == 1:
                        player.side = "Final"
                    elif start_input == "otworz szope":
                        print(
                            "Wchodzisz do szopy która wypełniona jest narzędziami. Znajdujesz w niej łom. Bierzesz go w nadziei że może się przydać:\n")
                        player.crowbar_status = True
                        player.eq.append("Łom")
                    elif start_input == "sprawdz szope":
                        print("Widzisz szopę może jest tam coś przydatnego:\n")
                    elif start_input == "idz polnoc":
                        print("Idziesz na północ:\n")
                        player.side = "Polnoc"
                        player.is_in_room = False
                    elif start_input == "idz wschod":
                        print("Widzisz dom ale nie dostrzegasz żadnego wejścia:\n")
                    elif start_input == "idz zachod":
                        print("Nic tutaj nie ma:\n")
                    elif start_input == "idz poludnie":
                        print("Idziesz na południe domu:\n")
                        player.side = "Poludnie"
                        player.is_in_room = False
                    elif start_input == "pomoc":
                        with open("src/help.txt") as f:
                            print(f.read() + "\n")
                    elif start_input == "wyjdz":
                        exit()
                    elif start_input == "dziennik":
                        print(str(player.journal))
                    elif start_input == "ekwipunek":
                        print("Ekwipunek: {}".format(player.eq))
                    elif start_input == "zycie":
                        print("Życie wynosi: {}".format(player.hp))
                    else:
                        print("nie znam takiej komendy podaj poprawna:\n")

                """East side of the home"""

                while player.side == "Wschod":
                    if not player.is_in_room:
                        East_home()
                        player.is_in_room = True
                    start_input = input().lower()
                    if player.evidence >= 3 and player.motive == 1:
                        player.side = "Final"
                    elif start_input == "sprawdz ogrodek":
                        print("Odgarniając dłonią ziemię z świeżo wykopanego dołu znajdujesz ludzkie części "
                              "ciała, które są w zaawansowanym stopniu rozkładu.")
                        if not "Zdjęcie ofiary z ogródka" in player.eq:
                            player.eq.append("Zdjęcie ofiary z ogródka")
                            print("Robisz zdjecie ofiary jako dowód")
                            player.eq.append("Zdjęcie ofiary z ogródka")
                            player.evidence = player.evidence + 1
                    elif start_input == "idz zachod":
                        print(
                            "Widzisz dom od wschodniej strony widać jedynie kilka zamkniętych okien. Nie jesteś "
                            "w stanie się do niego dostać:\n")
                    elif start_input == "idz wschod":
                        print("Nie ma tutaj nic ciekawego:\n")
                    elif start_input == "idz poludnie":
                        print("Idziesz na tył domu:\n")
                        player.side = "Poludnie"
                        player.is_in_room = False
                    elif start_input == "idz polnoc":
                        print("Idzesz na przód domu:\n")
                        player.side = "Polnoc"
                        player.is_in_room = False
                    elif start_input == "pomoc":
                        with open("src/help.txt") as f:
                            print(f.read() + "\n")
                    elif start_input == "wyjdz":
                        exit()
                    elif start_input == "dziennik":
                        print("Misje:\n -zdobądz dowody zbrodni: {}/3 \n -znajdź motyw zabójcy: {}/1\n".format(
                            player.evidence, player.motive))
                    elif start_input == "ekwipunek":
                        print("Ekwipunek: {}".format(player.eq))
                    elif start_input == "zycie":
                        print("Zycie wynosi: {}".format(player.hp))
                    else:
                        print("nie znam takiej komendy podaj poprawna:\n")

                while player.side == "Poludnie":
                    if not player.is_in_room:
                        South_home()
                        player.is_in_room = True
                    start_input = input().lower()
                    if player.evidence >= 3 and player.motive == 1:
                        player.side = "Final"
                    elif start_input == "sprawdz drzwi":
                        print("Widzisz zabarykadowane deską drzwi można je otworzyć jakimś "
                              "narzędziem:\n")
                    elif start_input == "otworz drzwi":
                        if player.crowbar_status:
                            print("Wywarzasz deske łomem i wchodzisz do domu")
                            player.side = "Dom_korytarz_gora"
                            player.door_status = True
                            player.is_in_room = False
                        else:
                            print("Nie mogę otworzyć tych drzwi potrzebuję jakiegoś narzędzia")
                    elif start_input == "idz zachod":
                        print("Idziesz na zachod domu")
                        player.side = "Zachod"
                        player.is_in_room = False
                    elif start_input == "idz wschod":
                        print("Idziesz na wschód domu:\n")
                        player.side = "Wschod"
                        player.is_in_room = False
                    elif start_input == "idz poludnie":
                        print("Widzisz zabarykadowane drzwi i po za tym nic ciekawego:\n")
                    elif start_input == "idz polnoc":
                        print("Nie ma tutaj nic ciekawego:")
                    elif start_input == "pomoc":
                        with open("src/help.txt") as f:
                            print(f.read() + "\n")
                    elif start_input == "wyjdz":
                        exit()
                    elif start_input == "dziennik":
                        print("Misje:\n -zdobądz dowody zbrodni: {}/3 \n -znajdź motyw zabójcy: {}/1\n".format(
                            player.evidence, player.motive))
                    elif start_input == "ekwipunek":
                        print("Ekwipunek: {}".format(player.eq))
                    elif start_input == "zycie":
                        print("Życie wynosi: {}".format(player.hp))
                    else:
                        print("nie znam takiej komendy podaj poprawna:\n")

                while player.side == "Dom_korytarz_gora":
                    if not player.is_in_room:
                        House_corridor_upper()
                        player.is_in_room = True
                    start_input = input().lower()
                    if player.evidence >= 3 and player.motive == 1:
                        player.side = "Final"
                    elif start_input == "sprawdz korytarz":
                        print("Nie ma tutaj nic ciekawego:\n")
                    elif start_input == "idz prawo":
                        print("Idziesz do sypialni:\n")
                        player.side = "sypialnia"
                        player.is_in_room = False
                    elif start_input == "idz lewo":
                        print("Idziesz do łazienki:\n")
                        player.side = "lazienka"
                        player.is_in_room = False
                    elif start_input == "idz tyl":
                        print("wychodzisz z domu:\n")
                        player.side == "Poludnie"
                        player.is_in_room = False
                    elif start_input == "idz prosto":
                        print("wchodzisz do kuchni:\n")
                        player.side = "kuchnia"
                        player.is_in_room = False
                    elif start_input == "pomoc":
                        with open("src/help.txt") as f:
                            print(f.read() + "\n")
                    elif start_input == "wyjdz":
                        exit()
                    elif start_input == "dziennik":
                        print("Misje:\n -zdobądź dowody zbrodni: {}/3 \n -znajdź motyw zabójcy: {}/1\n".format(
                            player.evidence, player.motive))
                    elif start_input == "ekwipunek":
                        print("Ekwipunek: {}".format(player.eq))
                    elif start_input == "zycie":
                        print("Życie wynosi: {}".format(player.hp))
                    else:
                        print("nie znam takiej komendy podaj poprawna:\n")

                while player.side == "kuchnia":
                    if not player.is_in_room:
                        Kitchen()
                        player.is_in_room = True
                    start_input = input().lower()
                    if player.evidence >= 3 and player.motive == 1:
                        player.side = "Final"
                    elif start_input == "sprawdz lodowka":
                        print("w lodówce znajdujesz głowę kobiety oraz inne części ciała.")
                        if not "Zdjecie z lodowki" in player.eq:
                            player.eq.append("Zdjecie z lodowki")
                            print("Robisz zdjecie glowy jako dowod")
                            player.evidence = player.evidence + 1
                    elif start_input == "idz tyl":
                        print("wychodzisz z kuchni:\n")
                        player.side = "Dom_korytarz_gora"
                        player.is_in_room = False
                    elif start_input == "sprawdz stol":
                        print(
                            "na stole znajduje się fotografia przedstawiającę Corbitta i Dziewczynę. Na tyle fotografii napisane jest “Ukochanemu tacie - Dolores”")
                        if not "zdjecie corki" in player.eq:
                            player.eq.append("zdjecie corki")
                            print("Bierzesz zdjecie corki jako dowod")
                            player.evidence = player.evidence + 1
                    elif start_input == "pomoc":
                        with open("src/help.txt") as f:
                            print(f.read() + "\n")
                    elif start_input == "wyjdz":
                        exit()
                    elif start_input == "dziennik":
                        print("Misje:\n -zdobądz dowody zbrodni: {}/3 \n -znajdź motyw zabójcy: {}/1\n".format(
                            player.evidence, player.motive))
                    elif start_input == "ekwipunek":
                        print("Ekwipunek: {}".format(player.eq))
                    elif start_input == "zycie":
                        print("Życie wynosi: {}".format(player.hp))
                    else:
                        print("nie znam takiej komendy podaj poprawna:\n")

                while player.side == "sypialnia":
                    if not player.is_in_room:
                        Bedroom()
                        player.is_in_room = True
                    start_input = input().lower()
                    if player.evidence >= 3 and player.motive == 1:
                        player.side = "Final"
                    elif start_input == "otworz szafka":
                        print(
                            "W szafce znajduje się pamiętnik Corbitta oprawiony w skórę")
                    elif start_input == "otworz pamietnik":
                        with open("src/diary.txt") as diary:
                            print(diary.read())
                            if not "Pamietnik Corbitta" in player.eq:
                                player.motive = True
                                print("znaleziono motyw zabojcy:\n")
                                player.eq.append("Pamietnik Corbitta")
                                print("Bierzesz pamietnik jako dowod")
                                player.evidence = player.evidence + 1
                    elif start_input == "idz tyl":
                        print("Wychodzisz na korytarz")
                        player.side = "Dom_korytarz_gora"
                        player.is_in_room = False
                    elif start_input == "pomoc":
                        with open("src/help.txt") as f:
                            print(f.read() + "\n")
                    elif start_input == "wyjdz":
                        exit()
                    elif start_input == "dziennik":
                        print("Misje:\n -zdobądz dowody zbrodni: {}/3 \n -znajdź motyw zabójcy: {}/1\n".format(
                            player.evidence, player.motive))
                    elif start_input == "ekwipunek":
                        print("Ekwipunek: {}".format(player.eq))
                    elif start_input == "zycie":
                        print("Zycie wynosi: {}".format(player.hp))
                    else:
                        print("nie znam takiej komendy podaj poprawna:\n")

                while player.side == "lazienka":
                    if not player.is_in_room:
                        Bathroom()
                        player.is_in_room = True
                    start_input = input().lower()
                    if player.evidence >= 3 and player.motive == 1:
                        player.side = "Final"
                    elif start_input == "sprawdz cialo":
                        print("Sprawdzasz cialo")
                        if not "Zdjęcie abominacji z łazienki" in player.eq:
                            print("Robisz zdjęcie abominacji jako dowód")
                            player.eq.append("Zdjęcie abominacji z łazienki")
                            player.evidence = player.evidence + 1
                    elif start_input == "idz tyl":
                        print("Wychodzisz na korytarz")
                        player.side = "Dom_korytarz_gora"
                        player.is_in_room = False
                    elif start_input == "idz tyl":
                        print("Wychodzisz na korytarz")
                        player.side = "Dom_korytarz_gora"
                        player.is_in_room = False
                    elif start_input == "pomoc":
                        with open("src/help.txt") as f:
                            print(f.read() + "\n")
                    elif start_input == "wyjdz":
                        exit()
                    elif start_input == "dziennik":
                        print("Misje:\n -zdobadz dowody zbrodni: {}/3 \n -znajdz motyw zabojcy: {}/1\n".format(
                            player.evidence, player.motive))
                    elif start_input == "ekwipunek":
                        print("Ekwipunek: {}".format(player.eq))
                    else:
                        print("nie znam takiej komendy podaj poprawna:\n")

                while player.side == "Final":
                    Final()
                    start_input = input().lower()
                    if start_input == "1":
                        print("Zostajesz zabity a swiat nigdy sie nie dowie prawdy")
                        start_input = input("Koniec Gry wcisnij Enter aby wyjsc")
                        exit()
                    elif start_input == "2":
                        if player.crowbar_status:
                            print("Atakujesz Corbitta łomem i:")
                            ran_int: int = randrange(100)
                            if ran_int < 70:
                                hp_ran: int = randrange(40)
                                print(
                                    "Udaje ci sie go wytracic z rownowagi i uciec a swiat dowie sie prawdy. Jednak jestes ranny i tracisz {} hp".format(
                                        hp_ran))
                                player.hp = player.hp - hp_ran
                                start_input = input("Koniec Gry wcisnij Enter aby wyjsc")
                                exit()
                            else:
                                print("Zostajesz zabity a swiat nigdy sie nie dowie prawdy")
                                exit()
                        else:
                            print("Atakujesz Corbitta piesciami:")
                            ran_int: int = randrange(100)
                            if ran_int < 30:
                                hp_ran: int = randrange(60)
                                print(
                                    "Udaje ci sie go wytracic z rownowagi i uciec a swiat dowie sie prawdy. Jednak jestes ranny i tracisz {} hp".format(
                                        hp_ran))
                                player.hp = player.hp - hp_ran
                                exit()
                            else:
                                print("Zostajesz zabity a swiat nigdy sie nie dowie prawdy")
                                start_input = input("Koniec Gry wcisnij Enter aby wyjsc")
                                exit()
                    elif start_input == "3":
                        if player.door_status:
                            print("Udaje ci uciec tylnymi drzwiami a swiat dowie sie prawdy")
                            start_input = input("Koniec Gry wcisnij Enter aby wyjsc")
                            exit()
                        else:
                            print(
                                "Uciekajac okazuje sie ze drzwi sa zablokowane. Corbitt ciebie zabija a swiat nie dowie sie prawdy")
                            start_input = input("Koniec Gry wcisnij Enter aby wyjsc")
                            exit()
        elif start_input == "pomoc":
            with open("src/help.txt") as f:
                start_input = input(f.read() + "\n").lower()
        else:
            start_input = input("nie znam takiej komendy podaj poprawna:\n").lower()